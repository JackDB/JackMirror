#!/usr/bin/env bash



echo -e "\e[0m"
echo '   ____ _     ____ ___  __  __ '
echo ' / ___| |   / ___/ _ \|  \/  |'
echo '| |  _| |  | |  | | | | |\/| |'
echo '| |_| | |__| |__| |_| | |  | |'
echo ' \____|_____\____\___/|_|  |_|'                              
echo -e "\e[0m"

echo -e "\e[91mMirror Software by Giacomo De Benedetto."
echo -e "\e[91mBased on MagicMirror2 by MichMich."
echo -e ""
echo -e "Avvio installazione........"

# Define the tested version of Node.js.
NODE_TESTED="v5.1.0"

# Determine which Pi is running.
ARM=$(uname -m) 

# Check the Raspberry Pi version.
if [ "$ARM" != "armv7l" ]; then
	echo -e "\e[91mHardware non supportato."
	echo -e "\e[91mSoftware installabile su RaspBerry Pi 2, 3, 3B o 3B+"
	exit;
fi

# Define helper methods.
function version_gt() { test "$(echo "$@" | tr " " "\n" | sort -V | head -n 1)" != "$1"; }
function command_exists () { type "$1" &> /dev/null ;}

# Update before first apt-get
echo -e "\e[96mAggiorno i pacchetti...\e[90m"
sudo apt-get update || echo -e "\e[91mAggiornamento fallito, continuo ugualmente.\e[90m"

# Installing helper tools
echo -e "\e[96mInstallo Helper Tools...\e[90m"
sudo apt-get --assume-yes install curl wget git build-essential unzip || exit

# Check if we need to install or upgrade Node.js.
echo -e "\e[96mControllo versione attuale Node.js ...\e[0m"
NODE_INSTALL=false
if command_exists node; then
	echo -e "\e[0mNode.js installato. Controllo la versione.";
	NODE_CURRENT=$(node -v)
	echo -e "\e[0mVersione minima richiesta: \e[1m$NODE_TESTED\e[0m"
	echo -e "\e[0mVersione installata.. \e[1m$NODE_CURRENT\e[0m"
	if version_gt $NODE_TESTED $NODE_CURRENT; then
		echo -e "\e[96mNode va aggiornato.\e[0m"
		NODE_INSTALL=true

		# Check if a node process is currenlty running.
		# If so abort installation.
		if pgrep "node" > /dev/null; then
			echo -e "\e[91mNode e' in esecuzione. Non posso aggiornarlo."
			echo "Per favore chiudi tutti i processi di Node e riavvia l'installazione."
			exit;
		fi

	else
		echo -e "\e[92mNo Aggiornamento di Node.js necessario.\e[0m"
	fi

else
	echo -e "\e[93mNode non e' installato.\e[0m";
	NODE_INSTALL=true
fi

# Install or upgrade node if necessary.
if $NODE_INSTALL; then
	
	echo -e "\e[96mInstallo Node.js...\e[90m"

	# Fetch the latest version of Node.js from the selected branch
	# The NODE_STABLE_BRANCH variable will need to be manually adjusted when a new branch is released. (e.g. 7.x)
	# Only tested (stable) versions are recommended as newer versions could break MagicMirror.
	
	NODE_STABLE_BRANCH="9.x"
	curl -sL https://deb.nodesource.com/setup_$NODE_STABLE_BRANCH | sudo -E bash -
	sudo apt-get install -y nodejs
	echo -e "\e[92mInstallazione di Node.js terminata.\e[0m"
fi

# Install MagicMirror
cd ~
if [ -d "$HOME/JackMirror" ] ; then
	echo -e "\e[93mSembra che il software sia gia' installato."
	echo -e "Per prevenire l'overwriting, l'installazione verra' interrotta."
	exit;
fi

echo -e "\e[96mClonazione Software dalla Repo di Jack...\e[90m"
echo -e "Inserire credenziali di accesso se richieste."
if git clone https://gitlab.com/JackDB/JackMirror.git; then 
	echo -e "\e[92mClonazione completata.\e[0m"
else
	echo -e "\e[91mErrore di rete. Impossibile accedere alla Repo."
	exit;
fi

cd ~/JackMirror  || exit
echo -e "\e[96mInstallo il software e le sue dipendenze...\e[90m"
if npm install; then 
	echo -e "\e[92mInstallazione comppletata.\e[0m"
else
	echo -e "\e[91mImpossibile completare l'installazione."
	exit;
fi

# Use sample config for start MagicMirror
cp config/config.js.sample config/config.js

# Check if plymouth is installed (default with PIXEL desktop environment), then install custom splashscreen.
echo -e "\e[96mCheck plymouth installation ...\e[0m"
if command_exists plymouth; then
	THEME_DIR="/usr/share/plymouth/themes"
	echo -e "\e[90mSplashscreen: Checking themes directory.\e[0m"
	if [ -d $THEME_DIR ]; then
		echo -e "\e[90mSplashscreen: Create theme directory if not exists.\e[0m"
		if [ ! -d $THEME_DIR/MagicMirror ]; then
			sudo mkdir $THEME_DIR/MagicMirror
		fi

		if sudo cp ~/MagicMirror/splashscreen/splash.png $THEME_DIR/MagicMirror/splash.png && sudo cp ~/MagicMirror/splashscreen/MagicMirror.plymouth $THEME_DIR/MagicMirror/MagicMirror.plymouth && sudo cp ~/MagicMirror/splashscreen/MagicMirror.script $THEME_DIR/MagicMirror/MagicMirror.script; then
			echo -e "\e[90mSplashscreen: Theme copied successfully.\e[0m"
			if sudo plymouth-set-default-theme -R MagicMirror; then
				echo -e "\e[92mSplashscreen: Changed theme to MagicMirror successfully.\e[0m"
			else
				echo -e "\e[91mSplashscreen: Couldn't change theme to MagicMirror!\e[0m"
			fi
		else
			echo -e "\e[91mSplashscreen: Copying theme failed!\e[0m"
		fi
	else
		echo -e "\e[91mSplashscreen: Themes folder doesn't exist!\e[0m"
	fi
else
	echo -e "\e[93mplymouth is not installed.\e[0m";
fi

# Use pm2 control like a service MagicMirror
read -p "Vuoi attivare pm2 per autoavviare il software (y/N)?" choice
if [[ $choice =~ ^[Yy]$ ]]; then
    sudo npm install -g pm2
    sudo su -c "env PATH=$PATH:/usr/bin pm2 startup linux -u pi --hp /home/pi"
    pm2 start ~/MagicMirror/installers/pm2_MagicMirror.json
    pm2 save
fi

echo " "
echo -e "\e[92mINSTALLAZIONE COMPLETATA CON SUCCESSO."
echo " "
echo " "
